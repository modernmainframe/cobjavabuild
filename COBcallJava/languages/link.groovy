import com.ibm.dbb.build.*

println("Linking COBOL program . . .")

def hlq = args[0]
def member = args[1]
def linkLog = args[2]

def lked = new MVSExec().pgm("IEWL").parm("RENT,LIST,LET,DYNAM(DLL),CASE(MIXED)")

lked.dd(new DDStatement().name("SYSLIB").dsn("CEE.SCEELKED").options("shr"))
lked.dd(new DDStatement().dsn("CEE.SCEELKEX").options("shr"))

lked.dd(new DDStatement().name("SYSLMOD").dsn("${hlq}.LOAD(${member})").options("shr"))

lked.dd(new DDStatement().name("OBJMOD").dsn("${hlq}.OBJ").options("shr"))

lked.dd(new DDStatement().name("SYSLIN").dsn("${hlq}.LINK(${member})").options("shr"))
lked.dd(new DDStatement().name("SYSPRINT").options("cyl space(5,5) unit(vio) blksize(133) lrecl(133) recfm(f,b) new"))
lked.copy(new CopyToHFS().ddName("SYSPRINT").file(new File("${linkLog}/${member}.lnklog")))

rc = lked.execute()

if (rc > 4) {
    throw new Exception("Return code is greater than 4! RC=$rc")
} else {
    println("Linking successful! RC=$rc")
}