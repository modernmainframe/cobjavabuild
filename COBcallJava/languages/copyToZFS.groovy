import com.ibm.dbb.build.*

println("Copying loadmodule to MVS ...")

def hlq = args[0]
def member = args[1]
def workspace = args[2]

def copyFile = new CopyToPDS()
copyFile.setFile(new File("${workspace}/${hlq}.LOAD"))
copyFile.setDataset("${hlq}.LOAD")
copyFile.setMember("${member}")
copyFile.copyMode(DBBConstants.CopyMode.LOAD)
copyFile.output(true)

rc = copyFile.execute()

if (rc > 4) {
    throw new Exception("Return code is greater than 4! RC=$rc")
} else {
    println("Copying loadmodule to MVS is successfull! RC=$rc")
}